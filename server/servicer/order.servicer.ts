import { Request, Response, NextFunction } from "express"
import { CatchAsyncError } from "../middlewares/catchAsyncError"
import OrderModel from "../models/order.model"

export const newOrder = CatchAsyncError(async (data: any, res: Response) => {
    const order = await OrderModel.create(data)

    res.status(201).json({
        success: true,
        order
    })
})

export const getAllOrdersService = async (res: Response) => {
    const Orders = await OrderModel.find().sort({ createdAt: -1 })
    res.status(201).json({
        success: true,
        Orders
    })

}