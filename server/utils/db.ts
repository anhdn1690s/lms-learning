import mongoose from 'mongoose'
require('dotenv').config()

const dbUlr: string = process.env.DB_URI || ""

const connectDB = async () => {
    try {
        await mongoose.connect(dbUlr).then((data: any) => {
            console.log(`Database connected with ${data.connection.host}`)
        })
    } catch (error: any) {
        console.log(`Database connected failed ${error.message}`)
        setTimeout(connectDB, 5000)
    };
}

export default connectDB