require('dotenv').config()
import express, { Request, Response, NextFunction } from "express"
import cors from "cors"
import cookieParser from "cookie-parser"
import { ErrorMiddleware } from "./middlewares/error"
import useRouter from "./routes/user.route"
import courseRouter from "./routes/course.route"
import orderRouter from "./routes/order.route"
import notificationRouter from "./routes/notification.route"
import analyticsRoute from "./routes/analytics.route"

export const app = express()

// body parser
app.use(express.json({ limit: "50mb" }))
// app.use(express.json())

//cookie parser
app.use(cookieParser())

app.use(cors({
    origin: ['http://localhost:3000'],
    credentials: true
}))

// route
app.use('/api/v1', useRouter)
app.use('/api/v1', courseRouter)
app.use('/api/v1', orderRouter)
app.use('/api/v1', notificationRouter)
app.use('/api/v1', analyticsRoute)


// test api
app.get('/test', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json({
        success: true,
        message: "API Good"
    })
})

app.all("*", (req: Request, res: Response, next: NextFunction) => {
    const err = new Error(`Route ${req.originalUrl} not found`) as any
    err.statusCode = 404
    next(err)
})

app.use(ErrorMiddleware)