import { getCourseAnalytics, getOrderAnalytics, getUserAnalytics } from "../controllers/analytics.controller";
import { createOrder, getAllOrder } from "../controllers/order.controller"
import { authorizeRoles, isAuthenticated } from "../middlewares/auth";

const analyticsRoute = require("express").Router();

analyticsRoute.get('/get-user-analytics', isAuthenticated, authorizeRoles("admin"), getUserAnalytics)
analyticsRoute.get('/get-course-analytics', isAuthenticated, authorizeRoles("admin"), getCourseAnalytics)
analyticsRoute.get('/get-order-analytics', isAuthenticated, authorizeRoles("admin"), getOrderAnalytics)

export default analyticsRoute