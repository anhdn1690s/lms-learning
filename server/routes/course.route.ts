import { addAnswer, addCourse, addQuestion, addReplyReview, addReview, deleteCourse, editCourse, generateVideoUrl, getAllCourse, getAllCourseAdmin, getCourseByUser, getSingleCourse, } from "../controllers/course.controller"
import { updateAccessToken } from "../controllers/user.controller";
import { authorizeRoles, isAuthenticated } from "../middlewares/auth";

const courseRouter = require("express").Router();

courseRouter.post('/add-course', updateAccessToken, isAuthenticated, authorizeRoles("admin"), addCourse)

courseRouter.put('/edit-course/:id', updateAccessToken, isAuthenticated, authorizeRoles("admin"), editCourse)

courseRouter.get('/course/:id', getSingleCourse)

courseRouter.get('/course', getAllCourse)

courseRouter.get('/course-content/:id', updateAccessToken, isAuthenticated, getCourseByUser)

courseRouter.post('/add-question', updateAccessToken, isAuthenticated, addQuestion)

courseRouter.post('/add-answer', updateAccessToken, isAuthenticated, addAnswer)

courseRouter.post('/add-review/:id', updateAccessToken, isAuthenticated, addReview)

courseRouter.post('/add-replyReview', updateAccessToken, isAuthenticated, authorizeRoles("admin"), addReplyReview)

courseRouter.post('/getVdoCipherOTP', generateVideoUrl)

courseRouter.get('/get-courses-admin', updateAccessToken, isAuthenticated, authorizeRoles("admin"), getAllCourseAdmin)

courseRouter.delete('/delete-course/:id', updateAccessToken, isAuthenticated, authorizeRoles("admin"), deleteCourse)

export default courseRouter