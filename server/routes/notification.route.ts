import { getNotification, updateStatusNotification } from "../controllers/notification.controller";
import { createOrder } from "../controllers/order.controller"
import { updateAccessToken } from "../controllers/user.controller";
import { authorizeRoles, isAuthenticated } from "../middlewares/auth";

const notificationRouter = require("express").Router();

notificationRouter.get('/notification', updateAccessToken, isAuthenticated, authorizeRoles('admin'), getNotification)
notificationRouter.put('/update-notification/:id', updateAccessToken, isAuthenticated, authorizeRoles('admin'), updateStatusNotification)


export default notificationRouter