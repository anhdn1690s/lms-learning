import { createOrder, getAllOrder } from "../controllers/order.controller"
import { updateAccessToken } from "../controllers/user.controller";
import { authorizeRoles, isAuthenticated } from "../middlewares/auth";

const orderRouter = require("express").Router();

orderRouter.post('/create-Order', isAuthenticated, createOrder)

orderRouter.get('/get-order', updateAccessToken, isAuthenticated, authorizeRoles("admin"), getAllOrder)
export default orderRouter