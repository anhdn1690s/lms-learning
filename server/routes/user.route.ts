import { registrationUser, activateUser, loginUser, logoutUser, updateAccessToken, getUserInfo, socialAuth, updateUserInfo, updatePassword, updateProfileAvatar, getAllUser, updateUserRole, deleteUser } from "../controllers/user.controller"
import { authorizeRoles, isAuthenticated } from "../middlewares/auth";

const useRouter = require("express").Router();

useRouter.post('/registration', registrationUser)

useRouter.post('/activate-user', activateUser)

useRouter.post('/login', loginUser)

useRouter.get('/logout', isAuthenticated, logoutUser)

useRouter.get('/refreshToken', updateAccessToken)

useRouter.get('/me', updateAccessToken, isAuthenticated, getUserInfo)

useRouter.post('/social-auth', socialAuth)

useRouter.put('/update-user-info', updateAccessToken, isAuthenticated, updateUserInfo)

useRouter.put('/update-password', updateAccessToken, isAuthenticated, updatePassword)

useRouter.put('/update-avatar', updateAccessToken, isAuthenticated, updateProfileAvatar)

useRouter.get('/get-user', updateAccessToken, isAuthenticated, authorizeRoles("admin"), getAllUser)

useRouter.put('/update-user-role', updateAccessToken, isAuthenticated, authorizeRoles("admin"), updateUserRole)

useRouter.delete('/delete-user/:id', updateAccessToken, isAuthenticated, authorizeRoles("admin"), deleteUser)

export default useRouter