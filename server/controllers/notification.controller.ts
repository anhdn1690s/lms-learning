import { Request, Response, NextFunction } from "express"
import { CatchAsyncError } from "../middlewares/catchAsyncError";
import ErrorHandler from "../utils/errorHandler";
import path from "path"
import ejs from "ejs"
import sendMail from "../utils/sendMail";
import Notification from "../models/notification.model";
import { CustomRequest } from "./user.controller";


//get Notification -- only for admin

export const getNotification = CatchAsyncError(async (req: Request, res: Response, next: NextFunction) => {
    try {
        const notification = await Notification.find().sort({ createdAt: -1 })
        res.status(200).json({
            success: true,
            notification
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// update status notification status --- only admin 

export const updateStatusNotification = CatchAsyncError(async (req: Request, res: Response, next: NextFunction) => {
    try {
        const notification = await Notification.findById(req.params.id)
        if (!notification) {
            return next(new ErrorHandler("Notification if not found", 400))
        } else {
            notification.status ? notification.status = "read" : notification?.status
        }

        await notification.save()

        const notifications = await Notification.find().sort({ createAd: -1 })

        res.status(200).json({
            success: true,
            notifications
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

//delete Notification 
var cron = require('node-cron');

cron.schedule("0 0 0 * * *", async () => {
    const thirtyDaysAgo = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000)
    await Notification.deleteMany({ status: "read", createdAt: { $lt: thirtyDaysAgo } })
    console.log("deleted read Notification",)
})