import { Request, Response, NextFunction } from "express"
import { CatchAsyncError } from "../middlewares/catchAsyncError";
import ErrorHandler from "../utils/errorHandler";
import { CustomRequest } from "./user.controller";
import { generateLast12MothsData } from "../utils/analytics.generator";
import userModel from "../models/use.model";
import CourseModel from "../models/course.model";
import OrderModel from "../models/order.model";



export const getUserAnalytics = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const users = await generateLast12MothsData(userModel)
        res.status(200).json({
            success: true,
            users
        })
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

export const getCourseAnalytics = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const course = await generateLast12MothsData(CourseModel)
        res.status(200).json({
            success: true,
            course
        })
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

export const getOrderAnalytics = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const order = await generateLast12MothsData(OrderModel)
        res.status(200).json({
            success: true,
            order
        })
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})