import { Request, Response, NextFunction } from "express"
import { CatchAsyncError } from "../middlewares/catchAsyncError";
import ErrorHandler from "../utils/errorHandler";
import cloudinary from "cloudinary"
import OrderModel, { IOrder } from "../models/order.model";
import userModel from "../models/use.model";
import CourseModel from "../models/course.model";
import path from "path"
import ejs from "ejs"
import sendMail from "../utils/sendMail";
import Notification from "../models/notification.model";
import { getAllOrdersService, newOrder } from "../servicer/order.servicer";
import { CustomRequest } from "./user.controller";

export const createOrder = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const { courseId, payment_info } = req.body as IOrder

        const user = await userModel.findById(req.user?._id)

        const courseExistsInUser = user?.courses.some((course: any) => course._id.toString() === courseId)

        // if (!courseExistsInUser) {
        //     return next(new ErrorHandler("you have already purchased this course", 400))
        // }

        const course = await CourseModel.findById(courseId)

        if (!course) {
            return next(new ErrorHandler("course not found", 400))
        }

        const data: any = {
            courseId: course._id,
            userId: user?._id,
            payment_info,
        }


        const mailData = {
            order: {
                _id: course._id.toString().slice(0, 6),
                name: course.name,
                price: course.price,
                date: new Date().toLocaleDateString("en-US", { year: 'numeric', month: 'long', day: 'numeric' })
            }
        }
        const html = await ejs.renderFile(path.join(__dirname, "../emails/order-confirmation.ejs"), { order: mailData })

        try {
            if (user) {
                await sendMail({
                    email: user.email,
                    subject: "Order Confirmation",
                    template: "order-confirmation.ejs",
                    data: mailData
                })
            }
        } catch (error: any) {
            return next(new ErrorHandler(error.message, 400))
        }

        user?.courses.push(course?._id)

        await user?.save()

        await Notification.create({
            user: user?._id,
            title: "New Order",
            message: `You have  a new order from ${course?.name}`
        })

        course.purchased ? course.purchased += 1 : course.purchased

        await course.save()

        newOrder(data, res, next)

    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// get all orders

export const getAllOrder = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        getAllOrdersService(res)
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})