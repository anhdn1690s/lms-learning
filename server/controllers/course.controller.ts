require('dotenv').config()
import { Request, Response, NextFunction } from "express"
import { CatchAsyncError } from "../middlewares/catchAsyncError";
import ErrorHandler from "../utils/errorHandler";
import cloudinary from "cloudinary"
import { createCourse, getAllCourseService } from "../servicer/course.servicer";
import userModel from "../models/use.model";
import CourseModel from "../models/course.model";
import { redis } from "../utils/redis";
import { CustomRequest } from "./user.controller";
import mongoose from "mongoose";
import path from "path";
import ejs from "ejs";
import sendMail from "../utils/sendMail";
import Notification from "../models/notification.model";
import axios from "axios";

// add course

export const addCourse = CatchAsyncError(async (req: Request, res: Response, next: NextFunction) => {
    try {
        const data = req.body
        const thumbnails = data.thumbnail
        if (thumbnails) {
            const myCloud = await cloudinary.v2.uploader.upload(thumbnails, {
                folder: "courses"
            })
            data.thumbnail = {
                public_id: myCloud.public_id,
                url: myCloud.secure_url
            }
        }
        createCourse(data, res, next)
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// edit course

export const editCourse = CatchAsyncError(async (req: Request, res: Response, next: NextFunction) => {
    try {
        const data = req.body

        const thumbnail = data.thumbnail

        const courseId = req.params.id

        const courseData = await CourseModel.findById(courseId) as any

        if (thumbnail && !thumbnail.startsWith("https")) {
            await cloudinary.v2.uploader.destroy(courseData.thumbnail.public_id)

            const myCloud = await cloudinary.v2.uploader.upload(thumbnail, {
                folder: "courses"
            })

            data.thumbnail = {
                public_id: myCloud.public_id,
                url: myCloud.secure_url
            }
        }

        if (thumbnail.startsWith("https")) {
            data.thumbnail = {
                public_id: courseData?.thumbnail.public_id,
                url: courseData?.thumbnail.url
            }
        }

        const course = await CourseModel.findByIdAndUpdate(courseId, { $set: data }, { new: true })

        res.status(200).json({
            success: true,
            course
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// get single course

export const getSingleCourse = CatchAsyncError(async (req: Request, res: Response, next: NextFunction) => {
    try {
        const courseId = req.params.id
        const isCacheExist = await redis.get(courseId)
        if (isCacheExist) {
            const course = JSON.parse(isCacheExist)
            res.status(200).json({
                success: true,
                course
            })
        } else {
            const course = await CourseModel.findById(req.params.id).select("-courseData.videoUrl -courseData.suggestion -courseData.links -courseData.questions")

            await redis.set(courseId, JSON.stringify(course), "EX", 604776)

            res.status(200).json({
                success: true,
                course
            })
        }

    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// get all course

export const getAllCourse = CatchAsyncError(async (req: Request, res: Response, next: NextFunction) => {
    try {
        const course = await CourseModel.find().select("-courseData.videoUrl -courseData.suggestion -courseData.links -courseData.questions")
        res.status(200).json({
            success: true,
            course
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// get course content -- only for valid user

export const getCourseByUser = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const userCourseList = req.user?.courses
        const courseId = req.params.id
        const courseExists = userCourseList?.find((course: any) => course._id.toString() === courseId)
        if (!courseExists) {
            return next(new ErrorHandler("Course is not found", 400))
        }
        const course = await CourseModel.findById(courseId)
        const content = course?.courseData
        res.status(200).json({
            success: true,
            content
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

//add question in course

interface IAddQuestionData {
    question: string
    courseId: string
    contentId: string
}

export const addQuestion = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const { question, contentId, courseId } = req.body as IAddQuestionData
        const course = await CourseModel.findById(courseId)
        if (!mongoose.Types.ObjectId.isValid(contentId)) {
            return next(new ErrorHandler("Invalid content id", 400))
        }
        const courseContent = course?.courseData?.find((item: any) => item._id.equals(contentId))
        if (!courseContent) {
            return next(new ErrorHandler("Invalid content id", 400))
        }

        const newQuestion: any = {
            user: req.user,
            question,
            questionReplies: []
        }

        courseContent.questions.push(newQuestion)

        await Notification.create({
            user: req.user?._id,
            title: "New Question",
            message: `You have  a new Question in ${courseContent.title}`
        })

        await course?.save()

        res.status(200).json({
            success: true,
            course
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

interface IAddAnswerData {
    answer: string
    courseId: string
    contentId: string
    questionId: string
}

export const addAnswer = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const { answer, courseId, contentId, questionId } = req.body as IAddAnswerData

        const course = await CourseModel.findById(courseId)

        if (!mongoose.Types.ObjectId.isValid(contentId)) {
            return next(new ErrorHandler("Invalid content id", 400))
        }

        const courseContent = course?.courseData?.find((item: any) => item._id.equals(contentId))

        if (!courseContent) {
            return next(new ErrorHandler("Invalid content id", 400))
        }

        const question = courseContent?.questions?.find((item: any) => item._id.equals(questionId))

        if (!question) {
            return next(new ErrorHandler("Invalid question id", 400))
        }

        const newAnswer: any = {
            user: req.user,
            answer
        }

        // question.questionReplies.push(newAnswer)

        if (question.questionReplies) {
            question.questionReplies.push(newAnswer)
        } else {
            question.questionReplies = [newAnswer]
        }

        await course?.save()


        if (req.user?._id === question.user._id) {
            await Notification.create({
                user: req.user?._id,
                title: "New question reply received",
                message: `You have a new question reply in ${courseContent.title}`
            })
        } else {
            const data = {
                name: question.user.name,
                title: courseContent.title
            }
            const html = await ejs.renderFile(path.join(__dirname, "../emails/question-reply.ejs"), data)
            try {
                await sendMail({
                    email: question.user.email,
                    subject: "Question reply",
                    template: "question-reply.ejs",
                    data
                })
            } catch (error: any) {
                return next(new ErrorHandler(error.message, 400))
            }
        }
        res.status(200).json({
            success: true,
            course
        })

    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// add review in course
interface IAddReview {
    review: string
    course: string
    rating: number
    useId: string
}

export const addReview = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const userCourseList = req.user?.course
        const courseId = req.params.id
        const courseExists = userCourseList?.some((course: any) => course._id.toString() === courseId.toString())

        if (!courseExists) {
            return next(new ErrorHandler("you are not aligible to access this courses", 400))
        }
        const course = await CourseModel.findById(courseId)

        const { review, rating } = req.body as IAddReview

        const reviewData: any = {
            user: req.user,
            comment: review,
            rating
        }

        course?.reviews.push(reviewData)

        let avg = 0

        course?.reviews.forEach((rev: any) => {
            avg += rev.ratings
        });

        if (course) {
            course.rating = avg / course.reviews.length
        }

        await course?.save()

        const notification = {
            title: "new review received",
            message: `${req.user?.name} has given a review in ${course?.name}`
        }

        res.status(200).json({
            success: true,
            course
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

// add reply review

interface IAddReplyReview {
    comment: string
    courseId: string
    reviewId: string
}

export const addReplyReview = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const { comment, courseId, reviewId } = req.body as IAddReplyReview

        const course = await CourseModel.findById(courseId)

        if (!course) {
            return next(new ErrorHandler('course not found', 400))
        }

        const review = course?.reviews.find((rev: any) => rev._id.toString === reviewId)

        if (!review) {
            return next(new ErrorHandler('review not found', 400))
        }

        const replyData: any = {
            user: req.user,
            comment

        }
        if (!review.commentReplies) {
            review.commentReplies = []
        }

        review.commentReplies?.push(replyData)

        await course?.save()

        res.status(200).json({
            success: true,
            course
        })
    }
    catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})



export const getAllCourseAdmin = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        getAllCourseService(res)
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})


export const deleteCourse = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const { id } = req.params
        const course = await CourseModel.findById(id)

        if (!course) {
            return next(new ErrorHandler("Course not found", 400))
        }
        await course.deleteOne({ id })
        await redis.del(id)

        res.status(200).json({
            success: true,
            message: "Course deleted successfully"
        })
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})

export const generateVideoUrl = CatchAsyncError(async (req: CustomRequest, res: Response, next: NextFunction) => {
    try {
        const { videoId } = req.body
        const response = await axios.post(`https://dev.vdocipher.com/api/videos/${videoId}/otp`,
            { ttl: 300 },
            {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: `Apisecret ${process.env.VDOCIPHER_API_SECRET}`
                }
            }
        )
        res.json(response.data)
    } catch (error: any) {
        return next(new ErrorHandler(error.message, 400))
    }
})