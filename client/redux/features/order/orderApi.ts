import { apiSlice } from "../api/apiSlice";

export const userApi = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getOrder: builder.query({
            query: () => ({
                url: "get-order",
                method: "GET",
                credentials: "include" as const
            }),
        }),
    })
})

export const { useGetOrderQuery } = userApi;
