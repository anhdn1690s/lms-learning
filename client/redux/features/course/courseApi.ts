import { apiSlice } from "../api/apiSlice";

export const courseApi = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        addCourse: builder.mutation({
            query: (data) => ({
                url: "add-course",
                method: "POST",
                body: data,
                credentials: "include" as const
            }),
        }),
        getAllCourse: builder.query({
            query: () => ({
                url: "get-courses-admin",
                method: "GET",
                credentials: "include" as const
            }),
        }),
        deleteCourse: builder.mutation({
            query: (id) => ({
                url: `delete-course/${id}`,
                method: "DELETE",
                credentials: "include" as const
            }),
        }),
        editCourse: builder.mutation({
            query: ({ id, data }) => ({
                url: `edit-course/${id}`,
                method: "PUT",
                body: data,
                credentials: "include" as const
            }),
        }),
    })
})

export const { useAddCourseMutation, useGetAllCourseQuery, useDeleteCourseMutation, useEditCourseMutation } = courseApi;
