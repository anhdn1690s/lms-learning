import { apiSlice } from "../api/apiSlice";

export const userApi = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        updateAvatar: builder.mutation({
            query: ({ avatar }) => ({
                url: "update-avatar",
                method: "PUT",
                body: { avatar },
                credentials: "include" as const
            }),
        }),
        updateUserInfo: builder.mutation({
            query: ({ name }) => ({
                url: "update-user-info",
                method: "PUT",
                body: { name },
                credentials: "include" as const
            }),
        }),
        updatePassword: builder.mutation({
            query: ({ oldPassword, newPassword }) => ({
                url: "update-password",
                method: "PUT",
                body: { oldPassword, newPassword },
                credentials: "include" as const
            }),
        }),
        getAllUser: builder.query({
            query: () => ({
                url: "get-user",
                method: "GET",
                credentials: "include" as const
            }),
        }),
    })
})

export const { useUpdateAvatarMutation, useUpdateUserInfoMutation, useUpdatePasswordMutation, useGetAllUserQuery } = userApi;
