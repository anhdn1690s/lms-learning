import { createSlice, PayloadAction } from "@reduxjs/toolkit"

const initialState = {
    token: "",
    user: "",
}

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        userRegistration: (state, action: PayloadAction<{ token: string }>) => {
            state.token = action.payload.token
        },
        userLoggedIn: (state, actions: PayloadAction<{ accessToken: string, user: string }>) => {
            state.token = actions.payload.accessToken
            state.user = actions.payload.user
        },
        userLoggedOut: (state, actions) => {
            state.token = ""
            state.user = ""
        }
    }
})

export const { userRegistration, userLoggedIn, userLoggedOut } = authSlice.actions;

export default authSlice.reducer