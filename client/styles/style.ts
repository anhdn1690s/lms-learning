export const styles = {
    title: "text-[25px] text-white font-[500] text-center py-2",
    label: "text-[16px] text-white",
    input: "w-full text-white bg-transparent border rounded h-[40px] px-2 outline-none mt-[10px] py-[10px]",
    button: "flex flex-row justify-center items-center py-3 px-6 rounded-full cursor-pointer bg-[#7F56D9] min-h-[45px] w-full text-[16px]"
}