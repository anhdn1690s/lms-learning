"use client";
import Header from "@/components/header";
import Heading from "@/utils/heading";
import React, { FC, useState } from "react";

interface Props {}

const Page: FC<Props> = (props) => {
  const [open, setOpen] = useState(false);
  const [activeItem, setActiveItem] = useState(0);
  const [route, setRoute] = useState("Login");
  return (
    <div>
      <Heading
        title="E-learning"
        description="E-learning is a platform for students to learn and get help from teachers"
        keywords="Programing, E-learning"
      />
      <Header
        open={open}
        activeItem={activeItem}
        setOpen={setOpen}
        setRoute={setRoute}
        route={route}
      />
    </div>
  );
};

export default Page;
