"use client";
import React, { FC } from "react";
import AdminProtected from "@/hooks/useAdminProtected";
import LayoutComponent from "@/components/layoutAdmin/layoutAdmin";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <AdminProtected>
      <LayoutComponent>{children}</LayoutComponent>
    </AdminProtected>
  );
}
