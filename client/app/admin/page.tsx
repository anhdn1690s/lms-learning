"use client";
import React from "react";
import AdminProtected from "@/hooks/useAdminProtected";
import LayoutComponent from "@/components/layoutAdmin/layoutAdmin";

const Page = () => {
  return <h1>Page Dashboard</h1>;
};

export default Page;
