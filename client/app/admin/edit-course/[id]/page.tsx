"use client";
import EditCourse from "@/components/admin/course/editCourse";
import React from "react";

type Props = {};

const Page = ({ params }: any) => {
  const id = params?.id;

  return (
      <EditCourse id={id} />
  );
};

export default Page;
