"use client";
import Header from "@/components/header";
import Profile from "@/components/profile";
import Protected from "@/hooks/useProtected";
import Heading from "@/utils/heading";
import React, { FC, useState } from "react";
import { useSelector } from "react-redux";

type Props = {};

const Page: FC<Props> = ({}) => {
  const [open, setOpen] = useState(false);
  const [activeItem, setActiveItem] = useState(0);
  const [route, setRoute] = useState("Login");
  const { user } = useSelector((state: any) => state.auth);
  return (
    <div>
      <Protected>
        <Heading
          title="Profile E-learning"
          description="E-learning is a platform for students to learn and get help from teachers"
          keywords="Programing, E-learning"
        />
        <Header
          open={open}
          activeItem={activeItem}
          setOpen={setOpen}
          setRoute={setRoute}
          route={route}
        />
        <Profile  user={user}/>
      </Protected>
    </div>
  );
};

export default Page;
