"use client";
import Link from "next/link";
import React from "react";

export const navItemData = [
  {
    name: "Home",
    url: "/",
  },
  {
    name: "Courses",
    url: "/courses",
  },
  {
    name: "About",
    url: "/about",
  },
  {
    name: "Policy",
    url: "/policy",
  },
];
type Props = {
  activeItem: number;
  isMobile: boolean;
};
const NavItem: React.FC<Props> = ({ activeItem, isMobile }) => {
  return (
    <>
      <div className="hidden 1024px:flex gap-[60px]">
        {navItemData &&
          navItemData.map((item, index) => (
            <Link
              href={`${item.url}`}
              key={index}
              passHref
              className={`${
                activeItem === index
                  ? "dark:text-[#37a39a] text-[#37a39a]"
                  : "dark:text-[#fff] text-[#FFF]"
              } text-[22px] font-[500] leading-[24px] font-DMSans`}
            >
              {item.name}
            </Link>
          ))}
      </div>
      {isMobile && (
        <div className="1024px:hidden mt-5px">
          <div className="w-full text-center py-[80px]">
            {navItemData &&
              navItemData.map((item, index) => (
                <Link
                  href={`${item.url}`}
                  passHref
                  key={index}
                  className={`${
                    activeItem === index
                      ? "dark:text-[#37a39a] text-[#37a39a]"
                      : "text-white"
                  } block text-[18px] font-[500] leading-[20px] font-DMSans py-[10px]`}
                >
                  {item.name}
                </Link>
              ))}
          </div>
        </div>
      )}
    </>
  );
};

export default NavItem;
