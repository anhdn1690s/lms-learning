"use client";
import React, { FC, useEffect, useState } from "react";
import Image from "next/image";
import logo from "../../assets/logo.svg";
import Link from "next/link";
import { HiOutlineMenuAlt3 } from "react-icons/hi";
import { AiOutlineClose } from "react-icons/ai";
import { BiUserCircle } from "react-icons/bi";
import NavItem from "./NavItem";
import { motion } from "framer-motion";
import ModalCustom from "../modal";
import Login from "../auth/Login";
import SignUp from "../auth/SignUp";
import Verification from "../auth/Verification";
import { useSelector } from "react-redux";
import userImg from "../../assets/user.png";
import { useSession } from "next-auth/react";
import {
  useLogOutQuery,
  useSocialAuthMutation,
} from "@/redux/features/auth/authApi";
import toast from "react-hot-toast";

type Props = {
  open: boolean;
  setOpen: (open: boolean) => void;
  activeItem: number;
  setRoute: (route: string) => void;
  route: string;
};
const Header: FC<Props> = ({ activeItem, setOpen, route, open, setRoute }) => {
  const [active, setActive] = useState(false);
  const [openSidebar, setOpenSidebar] = useState(false);
  const [logout, setLogout] = useState(false);

  const { user } = useSelector((state: any) => state.auth);
  const { data } = useSession();
  const [socialAuth, { isSuccess, error }] = useSocialAuthMutation();

  const {} = useLogOutQuery(undefined, {
    skip: !logout ? true : false,
  });

  useEffect(() => {
    if (!user) {
      if (data) {
        socialAuth({
          email: data?.user?.email,
          name: data?.user?.name,
          avatar: data?.user?.image,
        });
      }
    }
    if (isSuccess) {
      toast.success("Login Successfully");
    }
    if (!data === null) {
      setLogout(true);
    }
  }, [data, isSuccess, socialAuth, user]);

  if (typeof window !== "undefined") {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 80) {
        setActive(true);
      } else {
        setActive(false);
      }
    });
  }
  const handleClose = (e: any) => {
    if (e.target.id === "screen") {
      setOpenSidebar(false);
    }
  };

  return (
    <div className="w-full relative">
      <div
        className={`${
          active
            ? "dark:bg-opacity-50 dark:bg-gradient-to-b dark:from-gray-900 dark:to-black fixed top-0 left-0 right-0 w-full z-[80] border-b dark:border-[#ffff] shadow-xl transition duration-500"
            : "w-full border-b dark:border-[#ffffff1c] z-[80px] dark:shadow bg-[#2C1F4A]"
        }`}
      >
        <div className="w-[100%] m-auto py-[20px] h-full px-[20px] 1024px:w-[80%] 1024px:px-[0px] 1024px:py-[50px]">
          <div className="w-full flex items-center place-content-between">
            <Link
              href={"/"}
              className="flex items-center gap-[10px] 1024px:gap-[20px]"
            >
              <div className="rounded-[12.24px] p-[7px] bg-[#7F56D9]">
                <Image
                  src={logo}
                  alt="Img"
                  className="w-[30px] 1024px:w-full"
                />
              </div>
              <p className="font-Caladea text-[28px] font-[700] text-[#fff] leading-[40px] 1024px:text-[48px]">
                Edujar
              </p>
            </Link>
            <div className="flex gap-[20px] items-center 1024px:gap-[50px]">
              <NavItem activeItem={activeItem} isMobile={false} />

              {user ? (
                <Link href={"/profile"}>
                  <Image
                    src={user?.avatar ? user.avatar.url : userImg}
                    width={50}
                    height={50}
                    alt="Img"
                    className="w-[50px] h-[50px] rounded-full"
                  />
                </Link>
              ) : (
                <BiUserCircle
                  size={25}
                  className="cursor-pointer"
                  onClick={() => setOpen(true)}
                  color="#fff"
                />
              )}

              {/* only mobile*/}
              <div className="1024px:hidden">
                <HiOutlineMenuAlt3
                  size={25}
                  className="cursor-pointer"
                  onClick={() => setOpenSidebar(true)}
                  color="#fff"
                />
              </div>
            </div>
          </div>
        </div>
        {/* mobile nav */}
        {openSidebar && (
          <div
            className="fixed w-full h-screen top-0 left-0 z-[9999] bg-[#00000091]"
            onClick={handleClose}
            id="screen"
          >
            <motion.div
              initial={{ translateX: "100vw", opacity: 0 }}
              variants={{
                open: {
                  translateX: 0,
                  opacity: 1,
                },
                closed: {
                  translateX: "100vw",
                  opacity: 0,
                },
              }}
              transition={{ ease: "easeIn" }}
              animate={openSidebar ? "open" : "closed"}
              className="w-[70%] fixed z-[99999] h-screen bg-[#2C1F4A] top-0 right-0 ease-in-out duration-100"
            >
              <AiOutlineClose
                size={25}
                className="cursor-pointer absolute top-[30px] left-[30px]"
                onClick={() => setOpenSidebar(false)}
                color="#fff"
              />
              <NavItem activeItem={activeItem} isMobile={true} />
            </motion.div>
          </div>
        )}
      </div>
      {route === "Login" && (
        <>
          {open && (
            <ModalCustom
              open={open}
              setOpen={setOpen}
              setRouter={setRoute}
              activeItem={activeItem}
              component={Login}
            />
          )}
        </>
      )}
      {route === "Sign-Up" && (
        <>
          {open && (
            <ModalCustom
              open={open}
              setOpen={setOpen}
              setRouter={setRoute}
              activeItem={activeItem}
              component={SignUp}
            />
          )}
        </>
      )}
      {route === "Verification" && (
        <>
          {open && (
            <ModalCustom
              open={open}
              setOpen={setOpen}
              setRouter={setRoute}
              activeItem={activeItem}
              component={Verification}
            />
          )}
        </>
      )}
    </div>
  );
};

export default Header;
