import React, { FC, use, useEffect, useState } from "react";
import Image from "next/image";
import userImg from "../../assets/user.png";
import { AiOutlineCamera } from "react-icons/ai";
import { styles } from "@/styles/style";
import {
  useUpdateAvatarMutation,
  useUpdateUserInfoMutation,
} from "@/redux/features/user/userApi";
import toast from "react-hot-toast";
import { useLoadUserQuery } from "@/redux/features/api/apiSlice";

type Props = {
  avatar: string | null;
  user: any;
};

const ProfileInfo: FC<Props> = ({ avatar, user }) => {
  const [name, setName] = useState(user && user.name);
  const [loadUer, setLoadUser] = useState(false);

  const {} = useLoadUserQuery(undefined, {
    skip: loadUer ? true : false,
  });

  const [updateAvatar, { isSuccess, error }] = useUpdateAvatarMutation();
  const [updateUserInfo, { isSuccess: success, error: updateError }] =
    useUpdateUserInfoMutation();

  const imageHandler = async (e: any) => {
    const fileReader = new FileReader();

    fileReader.onload = () => {
      if (fileReader.readyState === 2) {
        const avatar = fileReader.result;
        updateAvatar(avatar);
      }
    };
    fileReader.readAsDataURL(e.target.files[0]);
  };

  useEffect(() => {
    if (success) {
      setLoadUser(true);
    }
    if(success) {
      toast.success("Update done");
    }
    if (error || updateError) {
      toast.error("Update not done");
    }
  }, [error, isSuccess, success, updateError]);

  const handlerSubmit = async (e: any) => {
    e.preventDefault();
    if (name !== "") {
      await updateUserInfo({
        name: name,
      });
    }
    
  };

  return (
    <>
      <div className="w-full flex justify-center">
        <div className="relative">
          <Image
            src={user?.avatar || avatar ? user.avatar.url || avatar : userImg}
            width={120}
            height={120}
            alt="Img"
            className="w-[120px] h-[120px] cursor-pointer border-[3px] rounded-full"
          />
          <input
            type="file"
            name=""
            id="avatar"
            className="hidden"
            onChange={imageHandler}
            accept="image/png,image/jpg,image/jpeg,image/webp"
          />
          <label htmlFor="avatar" className="relative">
            <div className="w-[30px] h-[30px] bg-slate-900 rounded-full absolute bottom-[0px] left-[0] flex items-center justify-center cursor-pointer">
              <AiOutlineCamera color="#fff" size={20} className="z-1" />
            </div>
          </label>
        </div>
      </div>
      <br />
      <div className="w-full pl-6 800px:pl-10">
        <form onSubmit={handlerSubmit}>
          <div className="800px:w-[50%] m-auto block pb-4">
            <div className="w-[100%]">
              <label className="block">Full Name</label>
              <input
                type="text"
                className={`${styles.input}  mb-4 800px:mb-0`}
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="w-[100%] pt-4">
              <label className="block ">Email</label>
              <input
                type="email"
                className={`${styles.input}  mb-4 800px:mb-0`}
                required
                value={user?.email}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <input
              type="submit"
              value="Update"
              required
              className={`${styles.button} mt-[40px]`}
            />
          </div>
        </form>
      </div>
    </>
  );
};

export default ProfileInfo;
