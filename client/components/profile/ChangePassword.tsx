import {
  useUpdatePasswordMutation,
} from "@/redux/features/user/userApi";
import { styles } from "@/styles/style";
import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";

const ChangePassword = () => {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [updatePassword, { isSuccess, error }] = useUpdatePasswordMutation();

  const passwordChangeHandler = async (e: any) => {
    e.preventDefault();
    if (newPassword !== confirmPassword) {
      toast.error("Password do not match");
    } else {
      await updatePassword({ oldPassword, newPassword });
    }
  };

  useEffect(() => {
    if (isSuccess) {
      toast.success("Password changed successfully");
    }
    if (error) {
      if ("data" in error) {
        const errorData = error as any;
        toast.error(errorData.data.message);
      }
    }
  }, [error, isSuccess]);

  return (
    <div className=" pl-7 px-2  800px:px-5 800px:pl-0 w-[800px] mx-auto">
      <h1 className="block  text-[25px] text-center font-[500] pb-2 text-white">
        Change Password
      </h1>
      <div className="w-full">
        <form
          onSubmit={passwordChangeHandler}
          className="flex flex-col items-center"
        >
          <div className="w-[100%]">
            <label className="block">Old password</label>
            <input
              type="password"
              className={`${styles.input}  mb-4 800px:mb-0`}
              required
              value={oldPassword}
              onChange={(e) => setOldPassword(e.target.value)}
            />
          </div>
          <div className="w-[100%] pt-4">
            <label className="block">New password</label>
            <input
              type="password"
              className={`${styles.input}  mb-4 800px:mb-0`}
              required
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
            />
          </div>
          <div className="w-[100%] pt-4">
            <label className="block">Confirm new password</label>
            <input
              type="password"
              className={`${styles.input}  mb-4 800px:mb-0`}
              required
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </div>
          <input
            type="submit"
            value="Update"
            required
            className={`${styles.button} mt-[40px]`}
          />
        </form>
      </div>
    </div>
  );
};

export default ChangePassword;
