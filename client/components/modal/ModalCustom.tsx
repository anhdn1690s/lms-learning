import React, { FC } from "react";
import { Modal, Box } from "@mui/material";
type Props = {
  open: boolean;
  setOpen: (open: boolean) => void;
  activeItem: any;
  component: any;
  setRouter?: (route: string) => void;
};
const ModalCustom: FC<Props> = ({
  open,
  setOpen,
  activeItem,
  component: Component,
  setRouter,
}) => {
  return (
    <div>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="absolute top-[50%] left-[50%] -translate-x-1/2 -translate-y-1/2 w-[450px] bg-[#2C1F4A] rounded-[8xp] shadow p-4  outline-none">
          <Component setOpen={setOpen} setRoute={setRouter} />
        </Box>
      </Modal>
    </div>
  );
};

export default ModalCustom;
