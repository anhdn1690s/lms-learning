import { useGetAllUserQuery } from "@/redux/features/user/userApi";
import { Box, Button } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import React, { FC, useEffect, useState } from "react";
import { AiOutlineDelete } from "react-icons/ai";
import Loader from "@/components/loader/Loader";
import { format } from "timeago.js";
import { styles } from "@/styles/style";
import { useGetAllCourseQuery } from "@/redux/features/course/courseApi";
import { useGetOrderQuery } from "@/redux/features/order/orderApi";

type Props = {};

const AllOrder: FC<Props> = () => {
  const [order, setOrder] = useState([]);

  const { isLoading, data: userData } = useGetAllUserQuery({});
  const { data: courseData } = useGetAllCourseQuery({});
  const { data: orderData } = useGetOrderQuery({});

  useEffect(() => {
    if (orderData) {
      const temp = orderData.Orders.map((item: any) => {
        const user = userData?.users.find(
          (user: any) => user._id === item.userId
        );
        const course = courseData?.courses.find(
          (course: any) => course._id === item.courseId
        );
        return {
          ...item,
          userName: user?.name,
          userEmail: user?.email,
          title: course?.name,
          price: "$" + course?.price,
        };
      });
      setOrder(temp);
    }
  }, [userData, courseData, orderData]);

  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "userName",
      headerName: "Name",
      flex: 0.5,
    },
    {
      field: "userEmail",
      headerName: "Email",
      flex: 0.6,
    },
    {
      field: "title",
      headerName: "course",
      flex: 0.6,
    },
    {
      field: "price",
      headerName: "Price",
      flex: 0.5,
    },
    {
      field: "create_at",
      headerName: "Create At",
      flex: 0.5,
    },
  ];

  const rows: any = [];
  {
    order &&
      order.forEach((item: any) => {
        rows.push({
          id: item._id,
          userName: item.userName,
          userEmail: item.userEmail,
          title: item.title,
          price: item.price,
          create_at: format(item.createdAt),
        });
      });
  }

  return (
    <div className="">
      {isLoading ? (
        <Loader />
      ) : (
        <Box m="20px">
          <div className=" flex justify-end">
            <div className={`${styles.button} !w-[300px] cursor-pointer`}>
              Add New Order
            </div>
          </div>
          <Box
            m="40px 0 0 0"
            height="80vh"
            sx={{
              "& .MuiDataGrid-cellContent ": { color: `#fff !important` },
              "& .MuiDataGrid-columnHeaders": { color: `#fff !important` },
              "& .MuiCheckbox-root": { color: `#fff !important` },
              "& .MuiSvgIcon-root": { fill: `#fff !important` },
              "& .MuiTablePagination-root ": { color: `#fff !important` },
            }}
          >
            <DataGrid checkboxSelection rows={rows} columns={columns} />
          </Box>
        </Box>
      )}
    </div>
  );
};

export default AllOrder;
