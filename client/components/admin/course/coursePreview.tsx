import { styles } from "@/styles/style";
import CoursePlayer from "@/utils/coursePlayer";
import Ratings from "@/utils/ratings";
import React, { FC } from "react";
import {
  IoMdCheckmarkCircle,
  IoMdCheckmarkCircleOutline,
} from "react-icons/io";

type Props = {
  active: number;
  setActive: (active: number) => void;
  courseData: any;
  handleCourseCreate: any;
  isEdit: boolean;
};

const CoursePreview: FC<Props> = ({
  active,
  setActive,
  courseData,
  handleCourseCreate,
  isEdit,
}) => {
  const disCountPercentage =
    ((courseData?.estimatedPrice - courseData?.price) /
      courseData?.estimatedPrice) *
    100;

  const disCountPresentencePrice = disCountPercentage.toFixed(0);

  const prevButton = () => {
    setActive(active - 1);
  };

  const nextButton = () => {
    handleCourseCreate();
  };

  return (
    <div className="w-[90%] m-auto py-5 mb-5">
      <div className="w-full relative">
        <div className="w-full mt-10">
          <CoursePlayer
            videoUrl={courseData?.demoUrl}
            title={courseData?.title}
          />
        </div>
        <div className="flex items-center">
          <h2 className="pt-5 text-[25px]">
            {courseData?.price === 0 ? "Free" : courseData?.price + "$"}
          </h2>
          <h5 className="px-3 text-[20px] mt-2 line-through opacity-80">
            {courseData?.estimatedPrice}$
          </h5>
          <h2 className="pl-5 pt-4 text-[20px]">
            {disCountPresentencePrice} % Off
          </h2>
        </div>
        <div className="flex items-center">
          <div
            className={`${styles.button} !w-[180px] my-3 !bg-[crimson] cursor-not-allowed`}
          >
            Buy Now {courseData?.price}
          </div>
        </div>
        <div className="flex items-center">
          <input
            type="text"
            name=""
            id=""
            placeholder="Discount Code.."
            className={`${styles.input} !w-[60%] !mt-0`}
          />
          <div
            className={`${styles.button} !w-[120px] my-3 ml-4 cursor-pointer`}
          >
            Apply
          </div>
        </div>
        <p className="pb-1">Source code included</p>
        <p className="pb-1">Full lifetime access</p>
        <p className="pb-1">Certificate of completion</p>
        <p className="pb-3">Premium Support</p>
        <div className="w-full">
          <div className="w-full 800px:pr-5">
            <h1 className="text-[25px]">{courseData?.name}</h1>
            <div className="flex items-center justify-between pt-3">
              <div className="flex items-center">
                <Ratings rating={0} />
                <h5>0 Review</h5>
              </div>
              <h5>0 Student</h5>
            </div>
            <br />
            <h1>What you will learn form this course</h1>
          </div>
          {courseData?.benefits?.map((item: any, index: number) => (
            <div className="w-full flex py-2" key={index}>
              <div className="w-[15px] mr-2">
                <IoMdCheckmarkCircleOutline size={20} />
              </div>
              <p className="pl-2">{item.title}</p>
            </div>
          ))}
          <br />
          <br />
          {courseData?.description}
          <br />
          <br />
          <div className="w-full flex items-center justify-between">
            <div
              className="w-full 800px:w-[180px] flex items-center justify-center h-[40px] bg-[#37a39a] text-center text-white rounded mt-8 cursor-pointer"
              onClick={() => prevButton()}
            >
              Prev
            </div>
            <div
              className="w-full 800px:w-[180px] flex items-center justify-center h-[40px] bg-[#37a39a] text-center text-white rounded mt-8 cursor-pointer"
              onClick={() => nextButton()}
            >
              {isEdit ? "Update" : "Create"}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CoursePreview;
