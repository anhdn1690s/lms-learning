import { styles } from "@/styles/style";
import React, { FC, useState } from "react";

type Props = {
  courseInfo: any;
  setCourseInfo: (courseInfo: any) => void;
  active: number;
  setActive: (active: number) => void;
};

const CourseInformation: FC<Props> = ({
  courseInfo,
  setCourseInfo,
  active,
  setActive,
}) => {
  const [dragging, setDragging] = useState(false);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    setActive(active + 1);
  };

  const handleFileChange = (e: any) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        if (reader.readyState === 2) {
          setCourseInfo({ ...courseInfo, thumbnail: reader.result });
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const handleDragOver = (e: any) => {
    e.preventDefault();
    setDragging(true);
  };

  const handleDragLeave = (e: any) => {
    e.preventDefault();
    setDragging(false);
  };

  const handleDrop = (e: any) => {
    e.preventDefault();
    setDragging(false);
    const file = e.dataTransfer.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        setCourseInfo({ ...courseInfo, thumbnail: reader.result });
      };
      reader.readAsDataURL(file);
    }
  };
  return (
    <div className="w-[80%] m-auto mt-24">
      <form onSubmit={handleSubmit} className={`${styles.label}`}>
        <div className="">
          <label htmlFor="">Course Name</label>
          <input
            type="text"
            name=""
            required
            value={courseInfo.name}
            onChange={(e) => {
              setCourseInfo({ ...courseInfo, name: e.target.value });
            }}
            id=""
            placeholder="Name Course"
            className={styles.input}
          />
        </div>
        <div className="mt-5">
          <label className={styles.label}>Course Description</label>
          <textarea
            name=""
            id=""
            cols={30}
            rows={10}
            placeholder="Course Description"
            className={`${styles.input} !h-min !py-2`}
            value={courseInfo.description}
            onChange={(e) => {
              setCourseInfo({ ...courseInfo, description: e.target.value });
            }}
          ></textarea>
        </div>
        <div className="mt-5 flex justify-between">
          <div className="w-[45%]">
            <label className={styles.label}>Course Price</label>
            <input
              type="number"
              id=""
              placeholder="Course Price"
              className={`${styles.input} !h-min !py-2`}
              value={courseInfo.price}
              onChange={(e) => {
                setCourseInfo({ ...courseInfo, price: e.target.value });
              }}
            ></input>
          </div>
          <div className="w-[50%]">
            <label className={styles.label}>Price Estimated (optional)</label>
            <input
              type="number"
              id=""
              placeholder="Course Price"
              className={`${styles.input} !h-min !py-2`}
              value={courseInfo.estimatedPrice}
              onChange={(e) => {
                setCourseInfo({
                  ...courseInfo,
                  estimatedPrice: e.target.value,
                });
              }}
            ></input>
          </div>
        </div>
        <div className="mt-5">
          <label className={styles.label}>Course Tags</label>
          <input
            type="text"
            id=""
            placeholder="Course Tags"
            className={`${styles.input}`}
            value={courseInfo.tag}
            onChange={(e) => {
              setCourseInfo({ ...courseInfo, tag: e.target.value });
            }}
          ></input>
        </div>
        <div className="mt-5 flex justify-between">
          <div className="w-[45%]">
            <label className={styles.label}>Course Level</label>
            <input
              type="text"
              id=""
              placeholder="Course Level"
              className={`${styles.input} !h-min !py-2`}
              value={courseInfo.level}
              onChange={(e) => {
                setCourseInfo({ ...courseInfo, level: e.target.value });
              }}
            ></input>
          </div>
          <div className="w-[50%]">
            <label className={styles.label}>Demo Url</label>
            <input
              type="text"
              id=""
              placeholder="Demo Url"
              className={`${styles.input} !h-min !py-2`}
              value={courseInfo.demoUrl}
              onChange={(e) => {
                setCourseInfo({
                  ...courseInfo,
                  demoUrl: e.target.value,
                });
              }}
            ></input>
          </div>
        </div>
        <br />
        <div className="w-full">
          <input
            type="file"
            accept="image/*"
            id="file"
            className="hidden"
            onChange={handleFileChange}
          />
          <label
            htmlFor="file"
            className={`w-full min-h-[10vh] border-[#00000026 p-3 border flex items-center justify-center ${
              dragging ? "bg-blue-500" : "bg-transparent"
            }]`}
            onDragOver={handleDragOver}
            onDragLeave={handleDragLeave}
            onDrop={handleDrop}
          >
            {courseInfo.thumbnail ? (
              <img
                src={courseInfo.thumbnail}
                alt="Img"
                className="max-h-full w-full object-cover"
              />
            ) : (
              <span className="text-white">
                Drag and drop your thumbnail here or click to browse
              </span>
            )}
          </label>
        </div>
        <br />
        <br />
        <div className="w-full flex items-center justify-end">
          <input type="submit" value="Next" className={styles.button} />
        </div>
      </form>
    </div>
  );
};

export default CourseInformation;
