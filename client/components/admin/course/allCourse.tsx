import Loader from "@/components/loader/Loader";
import {
  useDeleteCourseMutation,
  useGetAllCourseQuery,
} from "@/redux/features/course/courseApi";
import { styles } from "@/styles/style";
import { Box, Button, Modal } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { AiOutlineDelete } from "react-icons/ai";
import { FaRegEdit } from "react-icons/fa";
import { format } from "timeago.js";

type Props = {};

const AllCourse = (props: Props) => {
  const [open, setOpen] = useState(false);
  const [courseId, setCourseId] = useState("");
  const { isLoading, data, refetch } = useGetAllCourseQuery(
    {},
    { refetchOnMountOrArgChange: true }
  );

  const [deleteCourse, { isSuccess, error }] = useDeleteCourseMutation({});

  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "title",
      headerName: "Course Title",
      flex: 1,
    },
    {
      field: "ratings",
      headerName: "Ratings",
      flex: 0.5,
    },
    {
      field: "purchased",
      headerName: "Purchased",
      flex: 0.5,
    },
    {
      field: "create_at",
      headerName: "Create_at",
      flex: 0.5,
    },
    {
      field: "edit",
      headerName: "Edit",
      flex: 0.3,
      renderCell: (params: any) => {
        return (
          <>
            <Link href={`/admin/edit-course/${params.row.id}`}>
              <FaRegEdit className="" size={20} color="#fff" />
            </Link>
          </>
        );
      },
    },
    {
      field: "delete",
      headerName: "Delete",
      flex: 0.3,
      renderCell: (params: any) => {
        return (
          <>
            <Button
              onClick={() => {
                setOpen(!open);
                // setCourseId(courseId);
                setCourseId(params.row.id);
              }}
            >
              <AiOutlineDelete className="" size={20} color="#fff" />
            </Button>
          </>
        );
      },
    },
  ];
  const rows: any = [];

  {
    data &&
      data.courses?.forEach((item: any) => {
        rows.push({
          id: item._id,
          title: item.name,
          ratings: item.rating,
          purchased: item.purchased,
          create_at: format(item.createdAt),
        });
      });
  }

  useEffect(() => {
    if (isSuccess) {
      setOpen(false);
      refetch();
      toast.success("Course Deleted Successfully");
    }
    if (error) {
      if ("data" in error) {
        const errorMessage = error as any;
        toast.error(errorMessage.data.message);
      }
    }
  }, [error, isSuccess, refetch]);

  const handleDelete = async () => {
    const id = courseId;

    await deleteCourse(id);
  };
  return (
    <div className="">
      {isLoading ? (
        <Loader />
      ) : (
        <Box m="20px">
          <Box
            m="40px 0 0 0"
            height="80vh"
            sx={{
              "& .MuiDataGrid-cellContent ": { color: `#fff !important` },
              "& .MuiDataGrid-columnHeaders": { color: `#fff !important` },
              "& .MuiCheckbox-root": { color: `#fff !important` },
              "& .MuiSvgIcon-root": { fill: `#fff !important` },
              "& .MuiTablePagination-root ": { color: `#fff !important` },
            }}
          >
            <DataGrid checkboxSelection rows={rows} columns={columns} />
          </Box>
          {open && (
            <Modal
              open={open}
              onClose={() => setOpen(!open)}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box className="absolute top-[50%] left-[50%] -translate-x-1/2 -translate-y-1/2 px-[35px] py-[40px] rounded-[4px] bg-[#2C1F4A]">
                <h1 className={`${styles.title} !pb-[30px]`}>
                  Are you sure you want to delete this Course?
                </h1>
                <div className="flex w-full items-center justify-between mb-6">
                  <div
                    onClick={() => setOpen(!open)}
                    className={`${styles.button} !w-[120px] h-[50px] !bg-[green]`}
                  >
                    Cancel
                  </div>
                  <div
                    onClick={handleDelete}
                    className={`${styles.button} !w-[120px] h-[50px] !bg-[red]`}
                  >
                    Delete
                  </div>
                </div>
              </Box>
            </Modal>
          )}
        </Box>
      )}
    </div>
  );
};

export default AllCourse;
