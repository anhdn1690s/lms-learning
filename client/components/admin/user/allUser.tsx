import { useGetAllUserQuery } from "@/redux/features/user/userApi";
import { Box, Button } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import React, { FC, useState } from "react";
import { AiOutlineDelete } from "react-icons/ai";
import Loader from "@/components/loader/Loader";
import { format } from "timeago.js";
import { styles } from "@/styles/style";

type Props = {
  isTeam: boolean;
};

const AllUser: FC<Props> = ({ isTeam }) => {
  const [active, setActive] = useState(false);

  const { isLoading, data, error } = useGetAllUserQuery({});

  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
    },
    {
      field: "email",
      headerName: "Email",
      flex: 0.5,
    },
    {
      field: "role",
      headerName: "Role",
      flex: 0.5,
    },
    {
      field: "courses",
      headerName: "Purchased Courses",
      flex: 0.5,
    },
    {
      field: "create_at",
      headerName: "Create_at",
      flex: 0.5,
    },
    {
      field: "delete",
      headerName: "Delete",
      flex: 0.3,
      renderCell: () => {
        return (
          <>
            <Button>
              <AiOutlineDelete className="" size={20} color="#fff" />
            </Button>
          </>
        );
      },
    },
  ];

  const rows: any = [];
  if (isTeam) {
    const newData =
      data && data.users.filter((item: any) => item.role === "admin");
    newData &&
      newData.forEach((item: any) => {
        rows.push({
          id: item._id,
          name: item.name,
          email: item.email,
          role: item.role,
          courses: item.courses.length,
          create_at: format(item.createdAt),
        });
      });
  } else {
    data &&
      data.users.forEach((item: any) => {
        rows.push({
          id: item._id,
          name: item.name,
          email: item.email,
          role: item.role,
          courses: item.courses.length,
          create_at: format(item.createdAt),
        });
      });
  }

  return (
    <div className="">
      {isLoading ? (
        <Loader />
      ) : (
        <Box m="20px">
          <div className=" flex justify-end">
            <div className={`${styles.button} !w-[300px] cursor-pointer`}>
              Add New Member
            </div>
          </div>
          <Box
            m="40px 0 0 0"
            height="80vh"
            sx={{
              "& .MuiDataGrid-cellContent ": { color: `#fff !important` },
              "& .MuiDataGrid-columnHeaders": { color: `#fff !important` },
              "& .MuiCheckbox-root": { color: `#fff !important` },
              "& .MuiSvgIcon-root": { fill: `#fff !important` },
              "& .MuiTablePagination-root ": { color: `#fff !important` },
            }}
          >
            <DataGrid checkboxSelection rows={rows} columns={columns} />
          </Box>
        </Box>
      )}
    </div>
  );
};

export default AllUser;
