import React, { FunctionComponent, useState } from "react";
import AdminHeader from "../admin/header/adminHeader";
import AdminSidebar from "../admin/sidebar/adminSidebar";
import Heading from "@/utils/heading";

const LayoutComponent: FunctionComponent<{ children: any }> = ({
  children,
}) => {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  return (
    <>
      <Heading
        title="Admin E-learning"
        description="E-learning is a platform for students to learn and get help from teachers"
        keywords="Programing, E-learning"
      />
      
      <div className="flex h-screen overflow-hidden">
        <AdminSidebar
          sidebarOpen={sidebarOpen}
          setSidebarOpen={setSidebarOpen}
        />
        <div className="relative flex flex-1 flex-col overflow-y-auto overflow-x-hidden">
          <AdminHeader
            sidebarOpen={sidebarOpen}
            setSidebarOpen={setSidebarOpen}
          />
          <main>
            <div className="mx-auto max-w-screen-2xl p-4 md:p-6 2xl:p-10">
              {children}
            </div>
          </main>
        </div>
      </div>
    </>
  );
};

export default LayoutComponent;
